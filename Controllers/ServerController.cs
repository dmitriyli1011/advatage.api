using Microsoft.AspNetCore.Mvc;
using Advantage.API.Models;
using System.Linq;
using System.Collections.Generic;
using System;
using Microsoft.EntityFrameworkCore;

namespace Advantage.API.Controllers
{
    [Route("api/[controller]")]
    public class ServerController : Controller
    {
        private readonly ApiContext _ctx;

        public ServerController(ApiContext ctx)
        {
            _ctx = ctx;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var servers = _ctx.Servers.OrderBy(s => s.Id).ToList();

            return Ok(servers);
        }

        [HttpGet("{id}", Name = "GetServer")]
        public IActionResult GetServer(int id)
        {
            var server = _ctx.Servers.Find(id);
            return Ok(server);
        }

        [HttpPut("{id}")]
        public IActionResult Message(int id, [FromBody] ServerMessage msg)
        {
            var server = _ctx.Servers.Find(id);
            
            if(server==null)
            {
                return NotFound();
            }
            
            if(msg.Payload == "activate")
            {
                server.IsOnline = true;
            }

            if(msg.Payload == "deactivate")
            {
                server.IsOnline = false;
            }

            _ctx.SaveChanges();

            return new NoContentResult();
        }
    }
}