using Advantage.API.Models;
using System.Linq;
using System.Collections.Generic;
using System;

namespace Advantage.API
{
    public class DataSeed
    {
        private readonly ApiContext _context;
        public DataSeed (ApiContext ctx)
        {
            _context = ctx;
        }

        public void SeedData(int nCustomers, int nOrders)
        {
            if(!_context.Customers.Any())
            {
                SeedCustomers(nCustomers);
                 _context.SaveChanges();
            }

             if(!_context.Orders.Any())
            {
                SeedOrders(nOrders);
                 _context.SaveChanges();
            }

             if(!_context.Servers.Any())
            {
                SeedServers();
                 _context.SaveChanges();
            }
        }

        private void SeedCustomers(int n)
        {
            List<Customer> customers = BuildCustomerList(n);
            foreach(var customer in customers)
            {
                _context.Customers.Add(customer);
            }
        }

        private List<Customer> BuildCustomerList(int nCustomers)
        {
            var customers = new List<Customer>();
            var names = new List<string>();

            for(var i=1; i<=nCustomers; i++)
            {
                var name = Helpers.MakeUniqueCustomerName(names);
                names.Add(name);

                customers.Add(new Customer{
                    Id=i,
                    Name = name,
                    Email = Helpers.MakeCustomerEmail(name),
                    State = Helpers.GetRandomState()
                });
            }

            return customers;
        }

        private void SeedOrders(int n)
        {
            List<Order> orders = BuildOrderList(n);
            foreach(var order in orders)
            {
                _context.Orders.Add(order);
            }
        }

        private List<Order> BuildOrderList(int nOrders)
        {
            var orders = new List<Order>();

            Random rand = new Random();
            
            for(int i=1;i<=nOrders;i++)
            {
                var randCustomerId = rand.Next(1,_context.Customers.Count());

                var placed = Helpers.GetRandomOrderPlaced();
                var completed = Helpers.GetRandomOrderCompleted(placed);
                var customers = _context.Customers.ToList();

                orders.Add(new Order{
                    Id = i,
                    Customer =customers.First(c => c.Id == randCustomerId),
                    Total = Helpers.GetRandomOrderTotal(),
                    Placed = placed,
                    Completed = completed
                });
            }

            return orders;
        }

        private void SeedServers()
        {
            List<Server> Servers = BuildServerList();
            foreach(var server in Servers)
            {
                _context.Servers.Add(server);
            }
        }

        private List<Server> BuildServerList()
        {
            return new List<Server>()
            {
                new Server()
                {
                    Id = 1,
                    Name = "Dev-Web",
                    IsOnline = true
                },
                new Server()
                {
                    Id = 2,
                    Name = "Dev-Mail",
                    IsOnline = false
                },
                new Server()
                {
                    Id = 3,
                    Name = "Dev-Services",
                    IsOnline = true
                },
                new Server()
                {
                    Id = 4,
                    Name = "QA-Web",
                    IsOnline = true
                },
                new Server()
                {
                    Id = 5,
                    Name = "QA-Mail",
                    IsOnline = true
                },
                new Server()
                {
                    Id = 6,
                    Name = "QA-Services",
                    IsOnline = true
                },
                 new Server()
                {
                    Id = 7,
                    Name = "Prod-Web",
                    IsOnline = true
                },
                new Server()
                {
                    Id = 8,
                    Name = "Prod-Mail",
                    IsOnline = false
                },
                new Server()
                {
                    Id = 9,
                    Name = "Prod-Services",
                    IsOnline = true
                }
            };
        }
    }
}